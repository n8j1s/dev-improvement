// [
// 	[8,0,0,0,0,0,0,0,0]
// 	[0,0,3,6,0,0,0,0,0]
// 	[0,7,0,0,9,0,2,0,0]
// 	[0,5,0,0,0,7,0,0,0]
// 	[0,0,0,0,4,5,7,0,0]
// 	[0,0,0,1,0,0,0,3,0]
// 	[0,0,1,0,0,0,0,6,8]
// 	[0,0,8,5,0,0,0,1,0]
// 	[0,9,0,0,0,0,4,0,0]
// ]

// https://gizmodo.com/can-you-solve-the-10-hardest-logic-puzzles-ever-created-1064112665

package dev.improvement

import org.junit.*;
import spock.lang.*

class SudokuSolver extends Specification {

	SudokuChecker checker = new SudokuChecker();

	List<Integer> allVals = [1,2,3,4,5,6,7,8,9]
	List<Cell> cells = new ArrayList<>();
	List<Grid> subgrids
	List<List<Integer>> puzzle

	@Unroll
  	def 'easiest'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle = [
				  [1, 2, 3, 4, 5, 6, 7, 8, 9],
				  [4, 5, 6, 7, 8, 9, 1, 2, 3],
				  [7, 8, 9, 1, 2, 3, 4, 5, 6],
				  [2, 3, 4, 5, 6, 7, 8, 9, 1],
				  [5, 6, 7, 8, 9, 1, 2, 3, 4],
				  [8, 9, 1, 2, 3, 4, 5, 6, 7],
				  [3, 4, 5, 6, 7, 8, 9, 1, 2],
				  [6, 7, 8, 9, 1, 2, 3, 4, 5],
				  [9, 1, 2, 3, 4, 0, 6, 7, 8],
			  ]
			solve();
			done = System.currentTimeMillis();
			
		then:
			checker.isSolved(puzzle)
			println "done in " + (done - start) + " ms"
  	}

	@Unroll
  	def 'easy'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle =  [
					[0,4,6,8,3,2,0,5,0],
					[0,2,8,0,4,5,0,9,0],
					[0,0,0,1,9,6,2,0,0],
					[4,0,9,0,0,7,0,0,1],
					[3,5,0,9,0,0,0,7,2],
					[0,8,0,6,0,0,0,0,0],
					[7,0,0,5,0,0,0,2,0],
					[8,0,0,0,0,9,7,6,0],
					[6,0,0,3,7,0,4,0,5]
				]
			solve();
			done = System.currentTimeMillis();
		then:
			checker.isSolved(puzzle)
			println "done in " + (done - start) + " ms"
  	}

	@Unroll
  	def 'medium'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle =  [
					[0,4,0,0,2,7,0,0,0],
					[8,0,0,6,4,0,0,0,0],
					[0,7,0,0,3,0,9,0,8],
					[4,1,0,9,0,0,0,0,0],
					[0,0,3,0,0,5,0,0,2],
					[0,0,8,0,0,0,0,1,6],
					[0,8,4,0,0,0,0,0,0],
					[0,9,6,0,0,0,7,0,0],
					[1,5,0,4,9,0,8,2,0] 
				]
			solve();
			done = System.currentTimeMillis();
		then:
			checker.isSolved(puzzle)
			println "done in " + (done - start) + " ms"
  	}

	@Unroll
  	def 'hard'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle =  [
					[0,0,0,0,0,5,0,7,0],
					[1,0,0,0,0,0,0,4,9],
					[5,7,2,0,0,0,0,0,0],
					[0,0,8,0,6,0,0,0,7],
					[0,0,0,0,0,2,3,0,6],
					[9,0,0,5,0,7,0,0,0],
					[7,0,0,0,0,9,0,0,5],
					[0,9,0,0,5,0,0,0,0],
					[0,0,0,0,0,3,0,2,8] 
				]
			solve();
			done = System.currentTimeMillis();
		then:
			checker.isSolved(puzzle)
			println "done in " + (done - start) + " ms"
  	}

	@Unroll
  	def 'expert'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle =  [
					[0,0,0,0,0,0,0,0,0],
					[0,0,0,0,0,1,0,9,8],
					[0,0,5,7,0,0,0,3,0],
					[0,0,6,0,5,0,8,0,1],
					[0,0,0,0,6,0,0,0,0],
					[0,7,0,9,0,0,0,0,0],
					[0,6,8,0,0,0,4,0,7],
					[3,4,0,0,0,0,0,0,0],
					[0,9,0,5,0,2,6,0,0],
				]
			solve();
			done = System.currentTimeMillis();
		then:
			checker.isSolved(puzzle)
			println "done in " + (done - start) + " ms"
  	}

	@Unroll
  	def 'hardest ever in the history of the world'() {
		long start, done;
		when:
			start = System.currentTimeMillis();
			puzzle =  [
					[8,0,0,0,0,0,0,0,0],
					[0,0,3,6,0,0,0,0,0],
					[0,7,0,0,9,0,2,0,0],
					[0,5,0,0,0,7,0,0,0],
					[0,0,0,0,4,5,7,0,0],
					[0,0,0,1,0,0,0,3,0],
					[0,0,1,0,0,0,0,6,8],
					[0,0,8,5,0,0,0,1,0],
					[0,9,0,0,0,0,4,0,0]
				]
			solve();
			done = System.currentTimeMillis();
		then:
			checker.isSolved(puzzle)

			println "done in " + (done - start) + " ms"
  	}

	void solve(){
		for(int r = 0; r < 9; r++){
			for(int c = 0; c < 9; c++){
				Cell cell = new Cell(r, c);
				if(puzzle[r][c] > 0){
					cell.canChange = false;
					cell.currVal = puzzle[r][c]
					cell.possibleValues = Arrays.asList(puzzle[r][c]);
				}
				else
					setPossibleValues(cell, puzzle)
				cells.add(cell)
			}
		}

		// sort cells by number of possible values
		//cells.sort{a,b -> a.possibleValues.size().compareTo(b.possibleValues.size())}

		pickValue(cells[0], 0);
	}
	
	boolean pickValue(Cell c, int index){
		if(!c.canChange){
			if(index < cells.size() - 1)
				return pickValue(cells[index + 1], index + 1)
			return true
		} 

		setPossibleValues(c, puzzle)
		int counter = 0;
		if(c.possibleValues.size() == 0 )
			return false;
		while(counter++ < c.possibleValues.size()){
			
			c.currVal = c.possibleValues.remove(0);
			puzzle[c.row][c.col] = c.currVal
		
			if(index < cells.size() - 1){
				if(!pickValue(cells[index + 1], index + 1)){
					c.possibleValues.add(c.currVal);
					puzzle[c.row][c.col] = 0
				}
				else return true;
			}
			else 
				return true;
		}
		return false
	}

	void setPossibleValues(Cell p, List<List<Integer>> puzzle){
		p.possibleValues = new ArrayList<>(allVals);
		
		// remove any that already exist in row
		for(Integer v : puzzle[p.row])
			if(p.canChange && p.possibleValues.contains(v))
				p.possibleValues.remove(p.possibleValues.indexOf(v));

		// remove any that already exist in column
		for(List<Integer> row : puzzle) {
			if(p.canChange && p.possibleValues.size() > 0 && p.possibleValues.contains(row[p.col]))
				p.possibleValues.remove(p.possibleValues.indexOf(row[p.col]))
		}
		
		// remove any that already exist in subgrid
		int startingRow = 3*(Math.floor(p.row/3))
		int startingColumn = 3*(Math.floor(p.col/3))
		for(int i = startingRow; i <= startingRow +2; i++){
			for(int j = startingColumn; j <= startingColumn + 2; j++){
				if(p.canChange && p.possibleValues.size() > 0 && p.possibleValues.contains(puzzle[i][j]))
					p.possibleValues.remove(p.possibleValues.indexOf(puzzle[i][j]))
			}
		}
	}

	class Cell {
		int row;
		int col;
		boolean canChange = true;
		int currVal
		List<Integer> possibleValues = new ArrayList<>(allVals);

		public Cell(int r, int c){
			this.row = r;
			this.col = c;
		}
	}

	class Grid{
		List<Integer> valuesInGrid
	}
}