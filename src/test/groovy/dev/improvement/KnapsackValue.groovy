package dev.improvement

import spock.lang.*

/**
 * https://www.byte-by-byte.com/01knapsack/
 */
class KnapsackValue extends Specification {

  class Item{
    int weight;
    int value;
    public Item(int weight, int value){
      this.weight = weight;
      this.value = value;
    }
  }

  @Unroll
  def 'Value'(){
    when: 
      def items = [new Item(1, 6), new Item(3, 12), new Item(2, 10)];
      def weight = 5
    then:
      maxValue(items, weight) == 22
  }

  public static int maxValue(List<Item> items, int maxWeight) {

  }
}