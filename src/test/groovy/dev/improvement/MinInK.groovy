package dev.improvement

import spock.lang.*
class MinInK extends Specification {

	@Unroll
	def '934651,K1'(){
		when:
			def s = 934651
			def k = 1
		then:
			getMin(s, k) == 134659
	}

	@Unroll
	def '934651,K2'(){
		when:
			def s = 934651
			def k = 2
		then:
			getMin(s, k) == 134569
	}

	@Unroll
	def '52341,K2'(){
		// only 1 swap needed
		when:
			def s = 52341
			def k = 2
		then:
			getMin(s, k) == 12345
	}

	@Unroll
	def '12345,K2'(){
		// no swaps needed
		when:
			def s = 12345
			def k = 2
		then:
			getMin(s, k) == 12345
	}

	@Unroll
	def '845135182,K4'(){
		when:
			def s = 845135182
			def k = 4
		then:
			getMin(s, k) == 112345885
	}

	@Unroll
	def '18411,K2'(){
		when:
			def s = 18411
			def k = 2
		then:
			getMin(s, k) == 11148
	}

	@Unroll
	def '14811,K2'(){
		when:
			def s = 14811
			def k = 2
		then:
			getMin(s, k) == 11148
	}

	@Unroll
	def '14811,K1'(){
		when:
			def s = 14811
			def k = 1
		then:
			getMin(s, k) == 11814
	}

	def getMin(int s, int k){
		char[] digits = ("" + s).toCharArray();

		int count = 0;

		for(int i = 1; i <= 9 && count < k; i++){
			List<Integer> indexesOfI = getIndexOfValue(digits, i);
			List<Integer> leftMostIndexesGreaterThanI = getLeftMostIndexesGreaterThanI(digits, i, Math.min(indexesOfI.size(), k - count));

			if(indexesOfI.isEmpty())
				continue;

			for(Integer ind : indexesOfI.reverse()){
				if(count >= k)
					break;

				int indexOfLargestValue = -1;
				for(int j =0 ; j < leftMostIndexesGreaterThanI.size(); j++){
					if(indexOfLargestValue == -1 || digits[leftMostIndexesGreaterThanI[j]] > digits[indexOfLargestValue]){
						indexOfLargestValue = leftMostIndexesGreaterThanI[j]
					}
				}
		
				if(ind == -1 || indexOfLargestValue == -1 || ind < indexOfLargestValue)
					continue;
				swap(digits, ind, indexOfLargestValue)
				count++;
			}
		}

		return Integer.valueOf(new String(digits));
	}

	def  swap(char[] digits, int index1, int index2){
		// print "swapping " + index1 + " " + index2
		// int temp = digits[index1]
		// digits[index1] = digits[index2]
		// digits[index2] = temp
		// println " .... result: " + digits

		digits[index1] = digits[index1] ^ digits[index2]
		digits[index2] = digits[index1] ^ digits[index2]
		digits[index1] = digits[index1] ^ digits[index2]
	}

	def List<Integer> getLeftMostIndexesGreaterThanI(char[] digits, int value, int limit){
		List<Integer> toReturnIndex = new ArrayList<>();
		for(int i = 0; i < digits.length && toReturnIndex.size() < limit; i++){
			if(digits[i] > (value + ''))
				toReturnIndex.add(i);
		}
		return toReturnIndex;
	}

	def List<Integer> getIndexOfValue(char[] digits, int value) {
		//Get the smallest integer's index AND the best index to switch with
		//maybe change this to just return a new char array contaning the best new combination
		//c++ is basically the worst thing ever and even saruman would hate it
		List<Integer> toReturnIndex = new ArrayList<>();
		for(int i = 0; i < digits.length; i++) {
			if(digits[i] == (value + '')) {
				toReturnIndex.add(i)
			}
		}
		return toReturnIndex;
	}

	def int getBestSwitch(char[] digits, int index) {
		//get the best possible switch for index i
		char valueToMove = digits[index];
		for(int i = 0; i < index; i++) {
			if(valueToMove < digits[i])
				return i;
		}
		return -1;
	}
}