package dev.improvement

import spock.lang.*

class TreatsForCows extends Specification {

	def 'test 1'() {
		when:
			def treats = [1, 3, 1, 5, 2]
		then:
			maxValue(treats) == 43
  	}

	def maxValue(List<Integer> treats){
		int val = 0;
		int day = 1;
		while(treats.size() != 0) {
			if(treats[0] > treats[treats.size()-1]) {
				val += day * treats.remove(treats.size()-1);
			} else {
				val += day * treats.remove(0);
			}
			day++;
		}
		return val;
	}
}