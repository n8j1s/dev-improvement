package dev.improvement

import org.junit.*

class NextPalindrome {

	@Test
	void '808 -> 818'() {
		assert nextPal(808) == 818
  	}

	@Test
	void '2133 -> 2222'() {
		assert nextPal(2133) == 2222
  	}

	@Test
	void '81234 -> 81318'(){
		assert nextPal(81234) == 81318
	}

	@Test
	void '8999 -> 9009'(){
		assert nextPal(8999) == 9009
	}

	@Test
	void '8899 -> 8998'(){
		assert nextPal(8899) == 8998
	}

	def nextPal(int number){
		long start = System.currentTimeMillis();
		int next = number +1
		while(!isPal(next))
			next++
		long done = System.currentTimeMillis();

		println "elapsed time: " + (done - start)
		return next
	}

	def isPal(int number){
		String num = Integer.toString(number)
		return num.equals(new StringBuilder(num).reverse().toString())
	}
}