package dev.improvement

import spock.lang.*

class MinStepsAnagram extends Specification {

	def 'aba -> bab'() {
		when:
			def s = "bab"
	  		def t = "aba" 
		then:
			// replace an "a" with a "b" to make either "bba" or "abb"
			minSteps(s, t) == 1
  	}

	def 'practice -> leetcode'() {
		when:
			def s = "leetcode"
	  		def t = "practice"
		then:
			minSteps(s, t) == 5
	}

	def 'mangaar -> anagram'(){
		when:
			def s = "anagram"
	  		def t = "mangaar"
		then:
			minSteps(s, t) == 0
	}

	def 'xxyyzz -> xxyyzz'(){
		when:
			def s = "xxyyzz"
	  		def t = "xxyyzz"
		then:
			minSteps(s, t) == 0
	}

	def 'family -> friend'(){
		when:
			def s = "friend"
	  		def t = "family"
		then:
			minSteps(s, t) == 4
	}

	def minSteps(String s, String t){
		String sOrig = s;
		String tOrig = t;
		

		for(String sChar : s.toCharArray()){
			if(t.contains(sChar)){
				t = t.replaceFirst(sChar, "");
			}
		}

		return t.size()
	}
}