package dev.improvement

import spock.lang.*


class SudokuChecker extends Specification {

   @Unroll
  def 'test fail 1'(){
		when:
	  		def arr = [
				  [1, 2, 3, 4, 5, 6, 7, 8, 9],
				  [2, 3, 4, 5, 6, 7, 8, 9, 1],
				  [3, 4, 5, 6, 7, 8, 9, 1, 2],
				  [4, 5, 6, 7, 8, 9, 1, 2, 3],
				  [5, 6, 7, 8, 9, 1, 2, 3, 4],
				  [6, 7, 8, 9, 1, 2, 3, 4, 5],
				  [7, 8, 9, 1, 2, 3, 4, 5, 6],
				  [8, 9, 1, 2, 3, 4, 5, 6, 7],
				  [9, 1, 2, 3, 4, 5, 6, 7, 8],
			  ]
		then:
			!isSolved(arr)
  }

   @Unroll
  def 'test fail 2'(){
		when:
	  		def arr = [
				  [1, 2, 3, 4, 5, 6, 7, 8, 9],
				  [4, 5, 6, 7, 8, 9, 1, 2, 3],
				  [7, 8, 9, 1, 2, 3, 4, 5, 6],
				  [2, 3, 4, 5, 6, 7, 8, 9, 1],
				  [5, 6, 7, 8, 9, 1, 2, 3, 4],
				  [8, 9, 1, 2, 3, 4, 5, 6, 7],
				  [3, 4, 5, 6, 7, 8, 9, 1, 2],
				  [6, 7, 8, 9, 1, 2, 3, 4, 5],
				  [9, 1, 2, 3, 4, 5, 6, 7, 1],
			  ]
		then:
			!isSolved(arr)
  }

  @Unroll
  def 'test success'(){
		when:
	  		def arr = [
				  [1, 2, 3, 4, 5, 6, 7, 8, 9],
				  [4, 5, 6, 7, 8, 9, 1, 2, 3],
				  [7, 8, 9, 1, 2, 3, 4, 5, 6],
				  [2, 3, 4, 5, 6, 7, 8, 9, 1],
				  [5, 6, 7, 8, 9, 1, 2, 3, 4],
				  [8, 9, 1, 2, 3, 4, 5, 6, 7],
				  [3, 4, 5, 6, 7, 8, 9, 1, 2],
				  [6, 7, 8, 9, 1, 2, 3, 4, 5],
				  [9, 1, 2, 3, 4, 5, 6, 7, 8],
			  ]
		then:
			isSolved(arr)
  }

 	def isSolved(List<List<Integer>> puzzle){
		print(puzzle)
		// check every row
		for(int i = 0; i < 9; i++){
			if(!listCheck(puzzle[i]))
				return false;
		}
		// check every column
		for(int col = 0; col < 9; col++){
			List<Integer> columnVals = new ArrayList<>();
			for(int row = 0; row < 9; row++){
				columnVals.add(puzzle[row][col])
			}
			if(!listCheck(columnVals))
				return false;
		}

		// check every 3x3
		for(int gr = 0; gr < 3; gr++){
			for(int gc = 0; gc < 3; gc++){
				List<Integer> gridVals = new ArrayList<>();
				for(int i = gr*3; i < gr*3 + 3; i++){
					for(int j = gc*3; j < gc*3 + 3; j++){
						gridVals.add(puzzle[i][j]);
					}
				}
				if(!listCheck(gridVals))
					return false
			}
		}
		
		return true;
	}

	def listCheck(List<Integer> ints){
		for(int i = 1; i <= 9; i++){
			if(!ints.contains(i))
				return false;
		}
		return ints.size() == 9;
	}

	def print(List<List<Integer>> puzzle){
		for(List<Integer> row : puzzle){
			for(Integer col : row){
				print col + " "
			}
			println ""
		}
	}
}