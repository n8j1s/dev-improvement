from tile_state import Tile_State
from pprint import pprint
class Ship():

    """
    aircraft_carrier = 5
	destroyer = 3
	sub = 3
	battleship = 4
	patrol_boat = 2
    """

    def __init__(self, locations, name):
        #locations = [ (x,y), (x,y) ...]
        l = dict()
        for location in locations:
            l[location] = Tile_State.ship_part_undamaged
        self.locations = l
        self.name = name
        self.health = len(locations)

    def check_hit(self, location):
        #location = (x,y)
        return location in self.locations.keys()

    def take_hit(self, location):
        if self.check_hit(location):
            if self.locations[location] != Tile_State.ship_part_damaged:
                self.health -= 1
            self.locations[location] = Tile_State.ship_part_damaged
            return True
        return False

    def is_dead(self):
        return self.health == 0

if __name__ == "__main__":
    ship = Ship( [(0,0)], "blerg")
    pprint(ship.locations)

    """
    {
        (0, 0): <Tile_State.ship_part_undamaged: 1>
    }
    """