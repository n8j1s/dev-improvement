from pprint import pprint
from tile_state import Tile_State
from ship import Ship
from battleship_ai import BattleShip_Ai
from random import randint
from time import sleep


def get_origin_by_direction(direction, size, ship_size):
	# maybe have this check if the thing is valid
	if direction == 0:
		origin_point_x = randint(0, size-1)
		origin_point_y = randint(0, size-ship_size-1)
		return (origin_point_x, origin_point_y)
	else:
		origin_point_x = randint(0, size-ship_size-1)
		origin_point_y = randint(0, size-1)
		return (origin_point_x, origin_point_y)

def get_valid_ship_placement(ship_size, board, board_size):
	origin_point = -1
	direction = -1
	iscollision = True
	while iscollision:
		direction = randint(0, 1)
		origin_point = get_origin_by_direction(
			direction, board_size, ship_size)
		# origin_point = (x,y)
		# CHECK FOR COLLISIONS
		x = origin_point[0]
		y = origin_point[1]
		if direction == 0:
			iscollision = Tile_State.ship_part_undamaged in board[x][y:y+ship_size]
		else:
			for i in range(x, x + ship_size):
				
				# LIST INDEX OUT OF RANGE VVVV WHY
				if board[i][y] == Tile_State.ship_part_undamaged:
					iscollision = True
					break
				else:
					iscollision = False
	return (origin_point, direction)

def print_board(board):
	for row in board:
		for cell in row:
			print(cell.value, end=" | ")
		print()

def create_playing_board_with_ships(size):
	"""
	no stacking ships
	no going off the edge

	aircraft_carrier = 5
	destroyer = 3
	sub = 3
	battleship = 4
	patrol_boat = 2

	East:0
	South:1

	"""
	board = []
	ship_list = []
	for i in range(size):
		row = [Tile_State.nothing]*size
		board.append(row)

	ships = [(5, "Aircraft Carrier"), (4, "Battleship"), (3, "Destroyer"), (3, "Yellow Submarine"), (2, "Patrol Boat")]
	# ships should be its own class, replace this
	# origin_point = furthest left point
	# origin point = (x,y)
	for ship_size, ship_name in ships:
		origin_point, direction = get_valid_ship_placement(
			ship_size, board, size)
		x = origin_point[0]
		y = origin_point[1]
		# create the ship
		locations_list = list()
		for i in range(ship_size):
			if direction == 0:
				board[x][y+i] = Tile_State.ship_part_undamaged
				locations_list.append( (x, y+i) )
			else:
				board[x+i][y] = Tile_State.ship_part_undamaged
				locations_list.append( (x+i, y) )
		ship_list.append(Ship(locations_list, ship_name))
	#add hidden_board with has all 0s
	hidden_board = []
	for i in range(size):
		row = [Tile_State.nothing]*size
		hidden_board.append(row)
	return board, ship_list, hidden_board

def fire_at_location(ships, x, y):
	for ship in ships:
		if ship.take_hit((x,y)):
			return ship
	return None

def all_ships_dead(ships):
	for ship in ships:
		if not ship.is_dead():
			return False
	return True


if __name__ == "__main__":
	#NUKE IS BROKEN
	size = 10
	#player board
	player_true_board, player_ships, ai_playing_board = create_playing_board_with_ships(size)
	#ai board
	ai_true_board, ai_ships, player_playing_board = create_playing_board_with_ships(size)
	ai = BattleShip_Ai()
	player=0
	print_board(player_true_board)
	while True:
		#player round
		coord_to_hit = input("Pick a spot on the map to strike, Q to quit\n").lower()
		if coord_to_hit == "nuke":
			print("And thus a nuclear winter set in. Good work.")
			print('''
			_ ._  _ , _ ._
		  (_ ' ( `  )_  .__)
		( (  (    )   `)  ) _)
		(__ (_   (_ . _) _) ,__)
			`~~`\ ' . /`~~`
				;   ;
				/   \\
	_____________/_ __ \\_____________
			''')
			break
		if coord_to_hit == 'q':
			print("I embarrased that you would abandon the fight you so brazenly sought")
			break
		try:
			x,y = coord_to_hit.split(",")
			x = int(x)
			y = int(y)
			ship = fire_at_location(ai_ships, x, y)
			if ship:
				player_playing_board[x][y] = Tile_State.ship_part_damaged
				print("{} was hit".format(ship.name))
				print("...")
				#sleep(2)
				if ship.is_dead():
					#do we log kills by the player?
					print("AND DIED THE END")
					if all_ships_dead(ai_ships):
						print("PLAYER WINS")
						break
				else:
					print("BUT REMAINS AFLOAT")
			else:
				player_playing_board[x][y] = Tile_State.miss
				print("stop missing")
			print_board(player_playing_board)
		except ValueError:
			print("You screwed up your turn and you lose it")
			pass
		#ai round
		x,y = ai.determine_move(size, ai_playing_board)
		ship = fire_at_location(player_ships, x, y)
		if ship:
			ai_playing_board[x][y] = Tile_State.ship_part_damaged
			print("{} was hit".format(ship.name))
			print("...")
			ai.log_hit((x,y))
			#sleep(2)
			if ship.is_dead():
				print("AND DIED THE END")
				ai.killed_ship(ship)
				if all_ships_dead(player_ships):
					print("AI WINS")
					break
			else:
				print("BUT REMAINS AFLOAT")
		else:
			ai_playing_board[x][y] = Tile_State.miss
			print("ai stop missing")
		print_board(ai_playing_board)
	print_board(player_playing_board)
	print_board(ai_playing_board)