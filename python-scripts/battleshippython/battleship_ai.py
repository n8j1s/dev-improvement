from tile_state import Tile_State
from ship import Ship
from pprint import pprint
from random import randint


class BattleShip_Ai():
	# thorough <- weird spelled word
	"""
		Get a hit -> hit WSEN -> Once we get a second hit, follow that axis until a ship is sunk
		WSEN
		0123
	"""

	def __init__(self):
		self.hits = []  # [(x,y), (x,y)]
		self.current_direction = 0

	def determine_move(self, size, board):
		if len(self.hits) == 0:
			return self.choose_random_location(size, board)
		else:
			return self.choose_location(size, board)

	def choose_location(self, size, board):
		# We either need to use the self.hits
		# or if we can't we use chooserandom
		hit = self.hits[-1]
		viable_location = self.can_get_hit(size, board, hit)
		while viable_location is None:
			self.current_direction += 1
			if self.current_direction > 3:
				#potentially reconsider how hits are adding and removed
				self.current_direction = 0
				self.hits.pop()
				if len(self.hits) == 0:
					return self.choose_random_location(size, board)
				else:
					hit = self.hits[-1]
					viable_location = self.can_get_hit(size, board, hit)
			else:
				viable_location = self.can_get_hit(size, board, hit)
		return viable_location

	def choose_random_location(self, size, board):
		x = randint(0, size-1)
		y = randint(0, size-1)
		while board[x][y] != Tile_State.nothing:
			x = randint(0, size-1)
			y = randint(0, size-1)
		return x, y

	def translate_direction(self, hit):
		if self.current_direction == 0:
			return hit[0], hit[1] -1
		if self.current_direction == 1:
			return hit[0] + 1, hit[1]
		if self.current_direction == 2:
			return hit[0], hit[1] + 1
		if self.current_direction == 3:
			return hit[0] -1 , hit[1]
	
	def can_get_hit(self, size, board, hit):
		"""
		Checks if hit+direction is a viable location to hit
		^^^^^^^^^^^^^^^^^^^^^^^
		"""
		x,y = self.translate_direction(hit)
		if 0 <= x < size and 0 <= y < size and board[x][y] == Tile_State.nothing:
			return x,y
		else:
			return None

	def log_hit(self, hit):
		print(hit)
		self.hits.append(hit)

	def killed_ship(self, ship):
		locations = ship.locations
		print("HITS BEFORE MOVE")
		print(self.hits)
		print("REMOVE HITS")
		self.hits = list(set(self.hits) - set(locations))
		print(self.hits)

if __name__ == "__main__":
	#Test the AI
	list_one = [(3,2), (3,3), (3,4)]
	list_two = [(3,2), (4,4)]
	list_three = list(set(list_one) - set(list_two))
	print(list_three)
	list_three.remove((3,3))
	print(list_three)

